const connection = require("./connection");

const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split("=");
  acc[key.replace("--", "")] = value;
  return acc;
}, {});

const { id, table, query, textQuery, ...rest } = params;

let updateQuery = "";

updateQuery = `UPDATE ${table ? table : "newsposts"} SET `;
const updateValues = [];

for (const key in rest) {
  updateValues.push(`${key} = '${rest[key]}'`);
}

updateQuery += updateValues.join(", ");

if (!rest) {
  console.log(
    "Please provide either --id, --title, or --text to specify the record to update."
  );
  process.exit(1);
}

updateQuery += ` WHERE ${query} = '${textQuery}';`;

console.log("SQL Query:", updateQuery);

connection.query(updateQuery, (error, results) => {
  if (error) {
    throw error;
  }
  console.log(results);
});

connection.end();
