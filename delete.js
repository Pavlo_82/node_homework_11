const connection = require("./connection");

const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split('=');
  acc[key.replace('--', '')] = value;
return acc
}, {});

const { table, query, textQuery } = params;

if (!query && !textQuery) {
  console.log('Please provide either --query, or --textQuery to specify the record to delete.');
  process.exit(1);
}

let deleteQuery = `DELETE FROM ${table ? table : "newsposts"} WHERE ${query} = '${textQuery}';`;

console.log('SQL Query:', deleteQuery);

connection.query(deleteQuery, (error, results) => {
  if (error) {
    throw error;
  }
  console.log(results);
});
connection.end();