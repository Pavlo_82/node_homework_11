const connection = require("./connection");
const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split('=');
  acc[key.replace('--', '')] = value;
return acc
}, {});

const { table, insertData, ...customColumns } = params;
const tableName = table || 'newsposts';

const columns = Object.keys(customColumns);
const values = columns.map((column) => `${customColumns[column]}`).join(', ');

const str = insertData.slice(1,-1);
const dataArray = str.split(', ')

const array = Object.values(dataArray).map((item, index, array)=>{
  if(index===array.length-1){
    return `'${item}', NOW())`
  }
  if(index % 2) {
    return `'${item}', NOW()),`
  }
  if (!(index%2)){
    return `('${item}',`
  }
}).join(' ')

const insertQuery = `INSERT INTO ${tableName} (${values}, created_date)
                    VALUES ${array};`;
console.log(insertQuery, 5555);

connection.query(
  insertQuery,
  (error, results) => {
    if (error) {
      throw error;
    }
    console.log(results);
  }
);
connection.end();

