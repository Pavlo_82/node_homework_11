node createTable.js --title='VARCHAR(60)' --text='VARCHAR(1000)'

node insert.js --insertData='[New Post Title, New Post Text, Post 2 Title, Text for Post 2, Post 3 Title, Text for Post 3, Post 4 Title, Text for Post 4, Post 5 Title, Text for Post 5, Post 6 Title, Text for Post 6, Post 7 Title, Text for Post 7, Post 8 Title, Text for Post 8, Post 9 Title, Text for Post 9, Post 10 Title, Text for Post 10]' --table=newsposts --customColumn1='title' --customColumn2='text'

node delete.js --table=newsposts --query='title' --textQuery='Post 9 Title'

node getAll.js --table=newsposts --title=title

node getById.js --table=newsposts --id=5 --title=title --created_date=created_date

node getOne.js --table=newsposts --query='title' --queryText='Post 5 Title' --title=title --created_date=created_date

node update.js --table=newsposts --query='title' --textQuery='Post 5 Title' --title='hcnjmkmnhjmnf' --text='ggffdgfggtff'

node dropTable.js
